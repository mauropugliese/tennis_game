package it.uniba.tennisgame;

import static org.junit.Assert.*;


import org.junit.Test;

public class GameTest {

	@Test
	public void TestFifteenThirty() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void TestFortyThirty() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Federer forty - Nadal thirty", status);
	}

	
	@Test
	public void TestPlayer1wins() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Federer wins", status);
	}
	
	@Test
	public void TestFifteenForty() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Federer fifteen - Nadal forty", status);
	}
	
	@Test
	public void TestDeuce() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Deuce", status);
	}
	
	@Test
	public void TestAdvantagePlayer1() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Advantage Federer", status);
	}
	
	@Test
	public void TestPlayer2Wins() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Nadal wins", status);
	}
	
	
	@Test
	public void TestAdvantagePlayer2() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Advantage Nadal", status);
	}
	
	@Test
	public void TestDeuceAfterAdvantage() throws Exception {
		
		//arrange
		Game game = new Game ("Federer", "Nadal");
		String playerName1= game.getPlayerName1();
		String playerName2= game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//assert
		assertEquals("Deuce", status);
	}
}
