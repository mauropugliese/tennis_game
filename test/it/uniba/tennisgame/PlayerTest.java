package it.uniba.tennisgame;

import static org.junit.Assert.*;


import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldByIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1, player.getScore());
	
	
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		
		//Assert
		assertEquals(0, player.getScore());
	
	
	}
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		String scoreAsString = player.getScoreAsString();
				
		//Assert
		assertEquals("love", scoreAsString);
		
	
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player("Federer", 1);
		//Act
		String scoreAsString = player.getScoreAsString();
				
		//Assert
		assertEquals("fifteen", scoreAsString);
		
	
	}
	
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player("Federer", 2);
		//Act
		String scoreAsString = player.getScoreAsString();
				
		//Assert
		assertEquals("thirty", scoreAsString);
		
	
	}
	
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		String scoreAsString = player.getScoreAsString();
				
		//Assert
		assertEquals("forty", scoreAsString);
		
	
	}
	
	@Test
	public void scoreShouldNullIfNegative() {
		//Arrange
		Player player = new Player("Federer", -1);
		//Act
		String scoreAsString = player.getScoreAsString();
				
		//Assert
		assertNull (scoreAsString);
		
	
	}
	
	@Test
	public void scoreShouldNullIfMoreThanThree() {
		//Arrange
		Player player = new Player("Federer", 4);
		//Act
		String scoreAsString = player.getScoreAsString();
				
		//Assert
		assertNull (scoreAsString);
		
	
	}
	
	@Test
	public void ShouldBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie= player1.isTieWith(player2);
				
		//Assert
		assertTrue(tie);
		
	
	}

	@Test
	public void ShouldNotBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie= player1.isTieWith(player2);
				
		//Assert
		assertFalse(tie);
		
	
	}

	
	@Test
	public void ShouldHaveAtLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome= player1.hasAtLeastFortyPoints();
				
		//Assert
		assertTrue(outcome);
		
	
	}

	@Test
	public void ShouldNotHaveAtLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		//Act
		boolean outcome= player1.hasAtLeastFortyPoints();
				
		//Assert
		assertFalse(outcome);
		
	
	}
	
	
	@Test
	public void ShouldHaveLessThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		//Act
		boolean outcome= player1.hasLessThanFortyPoints();
				
		//Assert
		assertTrue(outcome);
		
	
	}
	
	
	@Test
	public void ShouldNotHaveLessThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome= player1.hasLessThanFortyPoints();
				
		//Assert
		assertFalse(outcome);
		
	
	}
	
	@Test
	public void ShouldHaveMoreThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		//Act
		boolean outcome= player1.hasMoreThanFourtyPoints();
				
		//Assert
		assertTrue(outcome);
		
	
	}
		
	
	@Test
	public void ShouldNotHaveMoreThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome= player1.hasMoreThanFourtyPoints();
				
		//Assert
		assertFalse(outcome);
		
	
	}
	
	
	
	@Test
	public void ShouldHaveOnePointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome= player1.hasOnePointAdvantageOn(player2);
				
		//Assert
		assertTrue(outcome);
		
	
	}
	

	@Test
	public void ShouldNotHaveOnePointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome= player1.hasOnePointAdvantageOn(player2);
				
		//Assert
		assertFalse(outcome);
		
	
	}
	
	
	
	
	@Test
	public void ShouldHaveAtLeastTwoPointsAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 5);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome= player1.hasAtLeastTwoPointsAdvantageOn(player2);
				
		//Assert
		assertTrue(outcome);
		
	
	}
	@Test
	public void ShouldNotHaveAtLeastTwoPointsAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean outcome= player1.hasAtLeastTwoPointsAdvantageOn(player2);
				
		//Assert
		assertFalse(outcome);
		
	
	}
	
}
 